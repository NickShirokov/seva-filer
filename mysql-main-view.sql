# Пишем адский запрос для основной таблицы - создаем вьюху
CREATE VIEW maintable AS
SELECT 
	# Идентификаторы
	#pmnts.id_payment,
	cntrcts.id_contract,  
	atmbls.id_auto,
	drvrs.id_driver,
	aststs.status_name,
	#Марка - модель - госномер	
	CONCAT(atmbls.mark,' ', atmbls.model, ' ', atmbls.gosnomer) as AutoMobile,
	cntrcts.date_start,
	#ФИО
	CONCAT(drvrs.first_name,' ', drvrs.middle_name, ' ', drvrs.last_name) as FIO,
	#Стоимость аренды
	cntrcts.arenda_stoim,	
	
	# Сумма внесенных по ареде
	
	IFNULL(SUM(pmnts.arenda), 0) as ArendaSumma,
	
	# Вся внесенная сумма
	
	
	IFNULL(SUM(pmnts.summa), 0) as MainSumma,
	
	#Количество дней аренды (оплаченных)
	ROUND(IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim) as ArendaDays,
	#Оплачен до
	
	/*
	DATE_SUB(cntrcts.date_start, INTERVAL - ROUND(IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim) DAY) as OplachenDo,	
	*/
	
	#Разность дат 
	DATEDIFF(CURRENT_DATE(), cntrcts.date_start) as FullDays,
	#Долг по аренде
	
	/*
	(DATEDIFF(CURRENT_DATE(), cntrcts.date_start) - ROUND(IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim)) * cntrcts.arenda_stoim AS DolgPoArende,
	
	*/
	
	#Оплатил по нашим штрафам + п.взнос + выкуп
	IFNULL(SUM(pmnts.pay_dogovor),0) as PayDogovorSumma,
	
	# Сумма штрафов текущего водителя	
	( 
		SELECT IFNULL(SUM(pen_summa), 0) 
		FROM our_penalties 
		WHERE p_contract_id = cntrcts.id_contract AND status_o_pen = 1
	) as SummaOurPenalties,

		
	# Сумма штрафов текущего водителя	+ пвзнос + выкуп
	((
		SELECT IFNULL(SUM(pen_summa),0) 
		FROM our_penalties 
		WHERE p_contract_id = cntrcts.id_contract ) 
		+ cntrcts.p_vznos + cntrcts.vykup
		
	) as SummaOurPenaltiesPvznosVykup,
	
	#Долг по договору
	((
		SELECT IFNULL(SUM(pen_summa),0) 
		FROM our_penalties 
		WHERE p_contract_id = cntrcts.id_contract ) 
		+ cntrcts.p_vznos + cntrcts.vykup
		
	) - IFNULL(SUM(pmnts.pay_dogovor),0) as DolgPoDogovoru,
	
	
	
	
	

	# Выборка суммы по штрафам ПДД для текущего водителя

	(
		SELECT IFNULL(SUM(summa_opl),0) 
		FROM pdd_penalties
		WHERE contract_id = cntrcts.id_contract 
		AND type_opl = 1
	
	) as SummaPddPenalties,

	
	#Оплатил по штрафам ПДД
	IFNULL(SUM(pmnts.pay_pdd),0) as PayPddSumma,
	
	# Долг по ПДД

	(
		SELECT IFNULL(SUM(summa_opl),0) 
		FROM pdd_penalties
		WHERE contract_id = cntrcts.id_contract 
		AND type_opl = 1	
	) - IFNULL(SUM(pmnts.pay_pdd),0) as DolgPddSummaReal,
	
	
	# Общий долг
	/*(
		SELECT IFNULL(SUM(summa_opl),0) 
		FROM pdd_penalties
		WHERE contract_id = cntrcts.id_contract 
		AND type_opl = 1	
	) - IFNULL(SUM(pmnts.pay_pdd),0) 
	
		+
	
	((
		SELECT IFNULL(SUM(pen_summa),0) 
		FROM our_penalties 
		WHERE p_contract_id = cntrcts.id_contract ) 
		+ cntrcts.p_vznos + cntrcts.vykup
		
	) - IFNULL(SUM(pmnts.pay_dogovor),0)	
	
		+ 
		
	(DATEDIFF(CURRENT_DATE(), cntrcts.date_start)
		- ROUND(IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim)) * cntrcts.arenda_stoim 
	
	AS MainDolg,
	*/	
		
	#Считаем остаток
	(
		IFNULL(SUM(pmnts.summa), 0) 
			- 
		#IFNULL(SUM(pmnts.arenda), 0)
		
		ROUND(IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim) * cntrcts.arenda_stoim
			- 
		IFNULL(SUM(pmnts.pay_dogovor),0)
		
	)
	

	AS Ostatok,
	
	#Считаем  перерасчет дней
	
	FLOOR((
		IFNULL(SUM(pmnts.summa), 0) 
			- 
		#IFNULL(SUM(pmnts.arenda), 0)
		
		ROUND(IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim) * cntrcts.arenda_stoim
			- 
		IFNULL(SUM(pmnts.pay_dogovor),0)
		
	) / cntrcts.arenda_stoim)
	
	AS PereraschetDays,
	
	
	#Считаем  перерасчет суммы аренды
	
	FLOOR((
		IFNULL(SUM(pmnts.summa), 0) 
			- 
		#IFNULL(SUM(pmnts.arenda), 0)
		
		ROUND(IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim) * cntrcts.arenda_stoim
			- 
		IFNULL(SUM(pmnts.pay_dogovor),0)
		
	) / cntrcts.arenda_stoim) * cntrcts.arenda_stoim
	
	AS PereraschetDaysArendSumma,
	
	
	
	#Считаем Новый остаток
	(
		IFNULL(SUM(pmnts.summa), 0) 
			- 
		#IFNULL(SUM(pmnts.arenda), 0)
		
		ROUND(IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim) * cntrcts.arenda_stoim
			- 
		IFNULL(SUM(pmnts.pay_dogovor),0)
		
	) - 
	
	FLOOR((
		IFNULL(SUM(pmnts.summa), 0) 
			- 
		#IFNULL(SUM(pmnts.arenda), 0)
		
		ROUND(IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim) * cntrcts.arenda_stoim
			- 
		IFNULL(SUM(pmnts.pay_dogovor),0)
		
	) / cntrcts.arenda_stoim) * cntrcts.arenda_stoim
	
	

	AS NewOstatok,
	
	
	
	
	#Новый Долг по аренде
	(
		DATEDIFF(CURRENT_DATE(), cntrcts.date_start) 
		- 
		ROUND(IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim)
	
	) * cntrcts.arenda_stoim 
		
		
		- 
		
		FLOOR((
			IFNULL(SUM(pmnts.summa), 0) 
				- 
			#IFNULL(SUM(pmnts.arenda), 0)
			
			ROUND(IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim) * cntrcts.arenda_stoim
				- 
			IFNULL(SUM(pmnts.pay_dogovor),0)
			
		) / cntrcts.arenda_stoim) * cntrcts.arenda_stoim
		
	
	AS DolgPoArende,
	
	
	# Новая дата оплачен до
	
	
	#Оплачен до
	DATE_SUB(cntrcts.date_start, 
		INTERVAL 
		- ROUND(IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim) 
		- (
			
		
			FLOOR((
			IFNULL(SUM(pmnts.summa), 0) 
				- 
			#IFNULL(SUM(pmnts.arenda), 0)
			
			ROUND(IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim) * cntrcts.arenda_stoim
				- 
			IFNULL(SUM(pmnts.pay_dogovor),0)
			
		) / cntrcts.arenda_stoim)
		
		
			)

	DAY) 
		
	as OplachenDo,

	
	
	
	
	
	

	# Новый общий долг
	
	
	
	
	
	
	
	# Общий долг
	(
		SELECT IFNULL(SUM(summa_opl),0) 
		FROM pdd_penalties
		WHERE contract_id = cntrcts.id_contract 
		AND type_opl = 1	
	) - IFNULL(SUM(pmnts.pay_pdd),0) 
	
		+
	
	((
		SELECT IFNULL(SUM(pen_summa),0) 
		FROM our_penalties 
		WHERE p_contract_id = cntrcts.id_contract ) 
		+ cntrcts.p_vznos + cntrcts.vykup
		
	) - IFNULL(SUM(pmnts.pay_dogovor),0)	
	
		+ 
		
	(DATEDIFF(CURRENT_DATE(), cntrcts.date_start)
		- ROUND(
		IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim)) * cntrcts.arenda_stoim 
		
		
		- 
		
		FLOOR((
			IFNULL(SUM(pmnts.summa), 0) 
				- 
			#IFNULL(SUM(pmnts.arenda), 0)
			
			ROUND(IFNULL(SUM(pmnts.arenda),0)/cntrcts.arenda_stoim) * cntrcts.arenda_stoim
				- 
			IFNULL(SUM(pmnts.pay_dogovor),0)
			
		) / cntrcts.arenda_stoim) * cntrcts.arenda_stoim
	
	AS MainDolg,
	
	
	
	
	
	
	
	
	IFNULL((
		SELECT SUM(arenda) 
		FROM payments 
		WHERE contract_id =  cntrcts.id_contract
		AND date_payment > (CURRENT_DATE() - INTERVAL 7 DAY)
	
	),0) AS WeekPayArenda, 
	
	
	
	IFNULL((
		SELECT SUM(arenda) 
		FROM payments 
		WHERE contract_id = cntrcts.id_contract 
		AND date_payment > (CURRENT_DATE() - INTERVAL 30 DAY)
	
	),0) AS MonthPayArenda
	
	
	
FROM contracts cntrcts 
LEFT JOIN payments pmnts
ON pmnts.contract_id = cntrcts.id_contract

LEFT JOIN automobiles atmbls 
ON cntrcts.auto_id = atmbls.id_auto 
LEFT JOIN auto_status aststs 
ON atmbls.status = aststs.id_status
LEFT JOIN drivers drvrs 
ON cntrcts.driver_id = drvrs.id_driver

GROUP BY cntrcts.id_contract;

 