<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Files;

use app\models\Filesdata;

/**
 * FilesSearchModel represents the model behind the search form of `app\models\Files`.
 */
class FilesSearchModel extends Files
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_file'], 'integer'],
            [['name_group', 'file_token', 'password_hash', 'status', 'date_create'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Files::find();

		$query->joinWith(['files_data']);
		
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_file' => $this->id_file,
            'date_create' => $this->date_create,
        ]);

        $query->andFilterWhere(['like', 'file_token', $this->file_token])
            ->andFilterWhere(['like', 'name_group', $this->name_group])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
