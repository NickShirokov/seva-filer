<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files_data".
 *
 * @property int $id_file
 * @property int $data_id
 * @property string $name_file
 */
class Filesdata extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'files_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data_id', 'name_file'], 'required'],
            [['data_id'], 'integer'],
            [['pass'], 'required'],
            [['name_file'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_file' => 'Id File',
            'data_id' => 'Data ID',
            'name_file' => 'Name File',
            'pass' => 'Пароль',
        ];
    }
}
