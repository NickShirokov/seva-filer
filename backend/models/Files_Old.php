<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property int $id_file
 * @property string $file_name
 * @property string $file_token
 * @property string $password_hash
 * @property string $status
 * @property string $date_create
 */
class Files extends \yii\db\ActiveRecord
{
	
	public $file_f;
	
	
	public $files_list;
	
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file_name', 'file_token', 'password_hash', 'status', 'date_create'], 'required'],
            [['status'], 'string'],
            [['date_create', 'file_name', 'name_group', 'files_list'], 'safe'],
            [[ 'file_token', 'password_hash'], 'string', 'max' => 255],
            [['id_file'], 'unique'],
			//[['file_f'], 'file',  'skipOnEmpty' => false, 'extensions' => 'jpg, png, jpeg, doc, docx, pdf, xls, xlsx, txt', 'maxFiles' => 20],
			[['file_f'], 'file', 'extensions' => 'jpg, png, jpeg, doc, docx, pdf, xls, xlsx, txt'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            //'id_file' => 'Id File',
            'file_f' => 'Файлы',
            'name_group' => 'Название пачки файлов',
            'files_list' => 'Список файлов',
			
            'file_token' => 'File Token',
            'password_hash' => 'Пароль',
            'status' => 'Статус',
            'date_create' => 'Дата создания',
        ];
    }
}
