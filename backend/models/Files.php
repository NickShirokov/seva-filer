<?php

namespace app\models;

use Yii;
use app\models\Filesdata;

/**
 * This is the model class for table "files".
 *
 * @property int $id_file
 * @property string $file_name
 * @property string $file_token
 * @property string $password_hash
 * @property string $status
 * @property string $date_create
 */
class Files extends \yii\db\ActiveRecord
{
	
	public $file_f;
	
	public $file_list;
	
	
	public $file_1;
	public $file_2;
	public $file_3;
	public $pass_1;
	public $pass_2;
	public $pass_3;
	
	
	
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file_token',  'status', 'date_create', 'name_group'], 'required'],
            [['pass_1',  'pass_2', 'pass_3'], 'safe'],
            [['status'], 'string'],
            [['date_create', 'file_name', 'file_list'], 'safe'],
            [[ 'file_token', 'password_hash'], 'safe'],
            [['id_file'], 'unique'],
			[['file_f'],'file', 'extensions' => 'jpg, png, jpeg, doc, docx, pdf, xls, zip, rar, xlsx, txt', 'maxFiles' => 10 ],
			[['file_1', 'file_3', 'file_3'],'file', 'extensions' => 'jpg, png, jpeg, doc, docx, pdf, xls, zip, rar, xlsx, txt'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            //'id_file' => 'Id File',
            'file_f' => 'Файлы',
            'file_1' => 'Файл 1',
            'file_2' => 'Файл 2',
            'file_3' => 'Файл 3',
            'pass_1' => 'Пароль  1',
            'pass_2' => 'Пароль 2',
            'pass_3' => 'Пароль 3',
			'name_group' => 'Название пачки файлов',
            'file_token' => 'Ссылка',
            'password_hash' => 'Пароль',
            'status' => 'Статус',
            'date_create' => 'Дата создания',
        ];
    }
	
	public function getfiles_data () {
		
		 return $this->hasMany(Filesdata::className(), ['data_id' => 'id_file']);
		
	}
	
	
}
