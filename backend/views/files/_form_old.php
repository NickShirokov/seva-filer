<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\Files */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="files-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    

    
	
    <?= $form->field($model, 'name_group')->textInput(['maxlength' => true]) ?>
	

	<div class="row">
     
	 <div class="col-md-4">
	 <?// $form->field($model, 'file_f[]')->fileInput(['multiple' => true, ]) ?>
	 <?= $form->field($model, 'file_f[]')->fileInput(); ?>
	
	
	<button class="reset-files">Очистить</button>
	</div>
	<div class="col-md-4">
		<div class="files-list">
		
		
		
		
		</div>
	
	</div>
	
	</div>
	
	
    <?= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'enable' => 'Enable', 'disable' => 'Disable', ]) ?>

    <?php 
	if ($model->date_create): 
	?>
	
			<?= $form->field($model, 'date_create')->input('date', ['required' => false]) ?>

	
	<?php else: ?>
	
	
		<?= $form->field($model, 'date_create')->input('date', ['required' => false, 'value' => date('Y-m-d')]) ?>

	
	<?php endif; ?>	
	
	</div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>







<?

$this->registerJs(
'


$(document).ready(function(){
	

	$("#files-file_f").change(function(){
				
		files = $(this)[0].files;
		
		data = "";

		$(".files-list").html(data);
		
		$.each(files,function(key, value){
				
			data = data+"<p class=\"p"+key+"\">"+value["name"]+"</p>";
		})
		
		$(".files-list").html(data);
		
		$(".reset-files").click(function(){
			
			
			$("#files-file_f")[0].value = "";
			$(".files-list").html("");
			
		}); 
		
		

	});

	
	
	
	
	
});


	
');


	
?>	
	