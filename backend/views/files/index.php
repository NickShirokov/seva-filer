<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FilesSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Files';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="files-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Files', ['create'], ['class' => 'btn btn-success']) ?>
		
		
		
		
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_file',
			
			
			/*
			[
				'attribute' => 'file_name',
				'format' => 'raw',
				'value' => function ($data) {
					
							
							$str = '<a target="_blank" href="/admin/uploads/'.$data['file_name'].'">'.$data['file_name'].'</a>';
							
							return $str;
						},
            ],
			*/
			
			
			//'file_token',
			'name_group',
			
			
			[
			
				'attribute' => 'file_list',
				'format' => 'raw',
				'value' => function($data) {
					
								$str = '';
					
								//print_r ($data);	
								foreach ($data['files_data'] as $item) {
									
									$str.= '<a target="_blank" href="/admin/uploads/'.$item['name_file'].'">'.$item['name_file'].'</a> 
									
												<b><span style="margin-left: 15px;">'.$item->pass.'</span><b>
												
												<br><br>';
							
									
									
								}
					
								return $str;
								
							},
				
			],
			
			
        //    'password_hash',
            'status',
            'date_create',
			[
				'attribute' => 'file_token',
				'format' => 'raw',
				'value' => function ($data) {
					
								$str = 'http://'.$_SERVER['HTTP_HOST'].'/site/files/?data='.$data['file_token'];
					
								return $str;
							},
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
