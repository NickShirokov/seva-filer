<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Files */

$this->title = $model->id_file;
$this->params['breadcrumbs'][] = ['label' => 'Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="files-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_file], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_file], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_file',
           // 'file_name',
			'name_group',
		   
            [
				'attribute' => 'file_token',
				'format' => 'raw',
				'value' => function ($data) {
								
								//print_r ($data);
								$str = 'http://'.$_SERVER['HTTP_HOST'].'/site/files/?data='.$data['file_token'];
					
								
								
							
								return $str;
				}
            ],
		//	'password_hash',
            'status',
            'date_create',
			[
				'attribute' => 'files',
				'format' => 'raw',
				'value' => function ($data) {
								
								
								//print_r ($data);	
								foreach ($data['files_data'] as $item) {
									
									$str.= '<a target="_blank" href="/admin/uploads/'.$item['name_file'].'">'.$item['name_file'].'</a>
									
									<b><span style="margin-left: 15px;">'.$item->pass.'</span><b>
									
									<br><br>';
							
									
									
								}
					
								return $str;
							}, 
			],
        ],
    ]) ?>

</div>
