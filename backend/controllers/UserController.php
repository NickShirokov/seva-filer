<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use backend\models\UserSearchModel;
use yii\filters\AccessControl;

use app\models\Authassignment;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


use backend\models\SignupForm;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
		//$userRole = Yii::$app->authManager->getRole('admin');
		//Yii::$app->authManager->assign($userRole, 1);
		
        return [
		
		
		[
				   'class' => AccessControl::className(),
				   'only' => ['index','create', 'update', 'delete'],
				   'rules' => [
						   [
								   'actions' => ['index','create', 'update', 'delete'],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'create', 'view', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			
		
		
		
			
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
		
		/*
		$role = Yii::$app->authManager->createRole('admin');
		$role->description = 'Админ';
		Yii::$app->authManager->add($role);
		 
		$role = Yii::$app->authManager->createRole('manager');
		$role->description = 'Юзер';
		Yii::$app->authManager->add($role);
		*/
		
		
        $searchModel = new UserSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	
	
	
	
	
	public function actionImport()
    {
		
		/*
		$role = Yii::$app->authManager->createRole('admin');
		$role->description = 'Админ';
		Yii::$app->authManager->add($role);
		 
		$role = Yii::$app->authManager->createRole('manager');
		$role->description = 'Юзер';
		Yii::$app->authManager->add($role);
		*/
		
		
        $searchModel = new UserSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('import', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	
	
	
	
	
	
	
	
	
	
	

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
			
			$post = Yii::$app->request->post();
				
	
			$type = $post['User']['type'];

			
			
			
			
			$a_ass_arr = Authassignment::find()->where(['user_id' => $model->id])->all();
			
			if (!empty($a_ass_arr)) {
				$a_ass = $a_ass_arr[0];
				
				$a_ass->item_name = $type;
				$a_ass->user_id = $model->id; 
				
			} else {
				$a_ass = new Authassignment ();
				$a_ass->item_name = $type;
				$a_ass->user_id = $model->id; 
			}
				
			$a_ass->save(false);
			
				
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

	
	
	
	
	/**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
				
			//Закодили сохранялку в базу с ролью пользователя
            if ($user = $model->signup()) {

			  
			    $post = Yii::$app->request->post();
			  
				$type = $post['SignupForm']['type'];
				$user_id = $user->id;
			
				$arr['item_name'] = $type;
				$arr['user_id'] = $user->id;
				
				$a_ass = new Authassignment ();
				
				$a_ass->item_name = $type;
				$a_ass->user_id = $user->id; 
					
				
				$a_ass->save(false);
				
				
				return $this->redirect(['user/index']);
				
            }
        }

		
		
        return $this->render('signup', [
            'model' => $model,
        ]);
		
		
    }
	
	
	
	
	
	
    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
