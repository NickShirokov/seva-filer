<?php

namespace backend\controllers;

use Yii;
use app\models\Files;
use backend\models\FilesSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

use app\models\Filesdata;

/**
 * FilesController implements the CRUD actions for Files model.
 */
class FilesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
		return [
		
			
            [
				   'class' => AccessControl::className(),
				   'only' => ['index','create', 'update', 'delete'],
				   'rules' => [
						   [
								   'actions' => ['index','create', 'update', 'delete'],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
					
					[
                        'actions' => ['logout', 'index', 'create', 'update', 'view', 'delete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
					
                ],
            ],
			
		
		
		
		
		
		
		
		
		
		
		
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Files models.
     * @return mixed
     */
    public function actionIndex()
	
	
    {
		
		
		/* Блок для скачивания файла */
		/*
		$file = "uploads/46299886.xlsx";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, "http://seva.arch/admin/".$file);
		$result = curl_exec($ch);
		curl_close($ch);
		 
		if ($result)
		{
			header('Content-type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' . $file);
			header('Content-Length: ' . strlen($result));
			echo $result;
			exit();  
		}
		*/
		
		
		
        $searchModel = new FilesSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Files model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Files model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Files();
		

        if ($model->load(Yii::$app->request->post()) ) {
			
			$post = Yii::$app->request->post();
			$t_m = 	$model;	
				
			$hash = Yii::$app->getSecurity()->generatePasswordHash(time().rand(1,1000));
			
			$model->file_token = $hash;
					
			$model->save(false);
						
			
			if (UploadedFile::getInstance($t_m ,'file_1')) {
				
				$t_m->file_1 = UploadedFile::getInstance($t_m ,'file_1');
				$t_m->file_1->saveAs('uploads/'.$t_m->file_1->baseName.'.'.$t_m->file_1->extension);
				$name_1 = $t_m->file_1->name;
				
				
				$model_files_data = new Filesdata();
				
				$model_files_data->data_id = $model->id_file;
				$model_files_data->pass = $model->pass_1;
				$model_files_data->name_file = $name_1;
				
				if (!empty($model_files_data->name_file)) {
					$model_files_data->save(false);
				}
				
			}
				
			if (UploadedFile::getInstance($t_m,'file_2')) {
			
			
				$model_files_data = new Filesdata();
				
				
				$t_m->file_2 = UploadedFile::getInstance($t_m,'file_2');
				$t_m->file_2->saveAs('uploads/'.$t_m->file_2->baseName.'.'.$t_m->file_2->extension);
				
				
				$name_2 = $t_m->file_2->name;
				
				$model_files_data->data_id = $model->id_file;
				$model_files_data->pass = $model->pass_2;
				$model_files_data->name_file = $name_2;
				
				if (!empty($model_files_data->name_file)) {
					$model_files_data->save(false);
				}
			
			}
			
			
			if (UploadedFile::getInstance($t_m,'file_3')) {
			
				$model_files_data = new Filesdata();
				
				$name_3 = $t_m->file_3->name;
						
				$t_m->file_3 = UploadedFile::getInstance($t_m,'file_3');
					
				$t_m->file_3->saveAs('uploads/'.$t_m->file_3->baseName.'.'.$t_m->file_3->extension);
		
				$model_files_data->data_id = $model->id_file;
				$model_files_data->pass = $model->pass_3;
				$model_files_data->name_file = $name_3;
				
				if (!empty($model_files_data->name_file)) {
					$model_files_data->save(false);
				}
			}
            return $this->redirect(['view', 'id' => $model->id_file]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Files model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_file]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Files model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Files model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Files the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Files::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
