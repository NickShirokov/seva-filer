<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

use app\models\Filesdata;

use app\models\Files;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
		
		
		
		
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'index'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
					
					[
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			
			
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		//return $this->redirect(['admin/index']);
		
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
		
	
		
        if ($model->load(Yii::$app->request->post())) {
			
			
            if ($user = $model->signup()) {
				
				
				$auth_key = $user->auth_key;
				$mail = $user->email;
				
				$link = '<a href="'.$_SERVER['SERVER_NAME'] . '/admin/validateregister?auth_key='.$auth_key.'">Ссылка для активации аккаунта</a>';
				
				mail($mail, 'GZ AUTO AUTH CODE', $link);
				
				return $this->render('signupsuccess', [
					'model' => $model,
				]);
				
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestpasswordreset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
	
	
	
	
	public function actionFiles ($data) {
			
		$model = new Files();	
			
			
		$models_files = Files::find()->where(['file_token' => $data])->all(); 
		
		if (empty($models_files)) {
			
			return $this->redirect(['site/error']);
			
		} else {
			
			
			if (Yii::$app->request->post()) {
				
				
				$post = Yii::$app->request->post();
				
				$pass = $post['Files']['password_hash'];
				
				
				$models_files = Files::find()->where(['file_token' => $data])->all(); 
				
				//print_r ($models_files);
				//exit;
				
				
				if (!empty($models_files)) {
					
					
					$links_arr = [];
					
					foreach ($models_files as $item) {
					
						$models_data = Filesdata::find()->where(['data_id' => $item->id_file, 'pass' => $pass ])->all();
						
					
						if (!empty($models_data)) {
							
							foreach ($models_data as $val) { 
							
								$links_arr[] = $val->name_file;
							
							}
					
					
						} else {
							
							return $this->redirect(['site/error']);
							
						}
						/*
					
						print_r ($models_data);
					
						exit;
					
					
						if (!empty($models_data)) {
							
							foreach ($models_data as $val) {
								
								$file = "uploads/".$val->name_file;
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_HEADER, 1);
								curl_setopt($ch, CURLOPT_FAILONERROR, 1);
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
								curl_setopt($ch, CURLOPT_URL, "http://seva.arch/admin/".$file);
								$result = curl_exec($ch);
								curl_close($ch);
								 
								if ($result)
								{
									header('Content-type: application/octet-stream');
									header('Content-Disposition: attachment; filename=' . $file);
									header('Content-Length: ' . strlen($result));
									echo $result;
									
								}
											
							}
							
							
							
						}
					
						*/
					
						
					
					
					}
					
					
				}
				
				
				return $this->render('filespage', [
					'model' => $model,
					'links_arr' => $links_arr,
				]);
				
				
				
			}
					
		}
		
		
        return $this->render('fileform', [
            'model' => $model,
        ]);
		
		
		
	}
	
	
}
