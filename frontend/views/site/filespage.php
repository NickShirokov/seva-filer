<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Файл';
$this->params['breadcrumbs'][] = 'Файл';
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Ваши файлы</p>

    <div class="row">
        <div class="col-lg-5">
		
		
		
			<?php 
			
				foreach ($links_arr as $key => $value) {
					
					
					echo '<a target="_blank" href="/admin/uploads/'.$value.'">'.$value.'</a><br>';
					
				}
			
			
			?>
		
           
        </div>
    </div>
</div>
