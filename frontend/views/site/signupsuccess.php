<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация завершена';
$this->params['breadcrumbs'][] = 'Регистрация завершена';
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <h2>Перейдите по ссылке полученной по указанному email, чтобы активировать учетную запись</h2>

    
	
	
</div>
