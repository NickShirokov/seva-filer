<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Файл';
$this->params['breadcrumbs'][] = 'Файл';
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Чтобы скачать файл введите пароль:</p>

    <div class="row">
        <div class="col-lg-5">
		
		
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                 <?= $form->field($model, 'password_hash')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'filebut']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
