<?php

namespace console\controllers;

use Yii;

use yii\console\Controller;
use console\models\Pddpenalties;

use console\models\Contracts;
use console\models\Pddpenstatus;
use console\models\Pddpenstatusapi;

use backend\models\PddpenaltiesSearchModel;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\UploadedFile;

use console\models\Autoparams;

use common\models\AutomobilesNewModel;
use common\models\DriversModel;

use console\models\Pddpenaltiesapi;
use console\models\Logitems;
use yii\filters\AccessControl;


//use yii\console\Controller;

use DateTime;


/**
 * PddpenaltiesController implements the CRUD actions for Pddpenalties model.
 */
class PddpenaltiesController extends Controller
{
   

    /**
     * Lists all Pddpenalties models.
     * @return mixed
     */
    public function actionIndex()
    {
		echo "Hello";
		$model = new Pddpenalties();
	
		//Все штрафы до 10 февраля включительно, которые добавились
		//$api_models = Pddpenaltiesapi::find()->where(['<=', 'created_at', '2020-02-10'])->orderBy('created_at DESC')->all();
		
		
		$api_models = Pddpenaltiesapi::find()->orderBy('created_at DESC')->all();
		
	
		foreach ($api_models as $item) {
			
			
			$sts = $item->vehicle_certificate;
			
			
			
			
			
			
			$summa_opl = $item->price;
			
			//$type_opl = $item->violator_paid;
			
			if ($item->violator_paid == 0) {
				
				$type_opl = 1;
				
			} else {
				
				$type_opl = 2;
				
			}
			
			$date_post = $item->ruling_date;
			
			$date_narush = $item->violation_date;
			
			$n_post = $item->ruling_number;
			
			
			if (strtotime($date_narush) > strtotime('31.12.2019')) {
				
				$type_opl = 1;
				
			}
			
			
			
			
			
			$narushitel = $item->violator;
			
			
			
			$type_opl_api = $item->paid;
			
			if ($item->paid == 0) {
				
				$type_opl_api = 1;
				
			} else {
				
				$type_opl_api = 3;
				
			}
			
			
			
			
			
			
			
			$skidka_status = 0;
			
			
			if ($summa_opl > 5000) {
				
				$skidka_status = 1;
			}
			
			if ($summa_opl == 2000) {
				
				$skidka_status = 1;
			}
			
			
			
			
			//ПРобуем найти машину по стс
			$model_autoparams = Autoparams::find()->where(['param_sts' => $sts])->all();
			
	
			
			$model_pens = Pddpenalties::find()->where(['n_post' => $n_post])->all();
			
			$id_shtraf = $model_pens[0]->id_shtraf;
			
			//Проверка наличия штрафы
			
			
			
			
			if (!empty($id_shtraf)) {
				
				
				$auto_id = $model_autoparams[0]->auto_id;
					
					
				$id_param = $model_autoparams[0]->id_param;
				
				//
				$model = $this->findModel($id_shtraf);
				
				if ($summa_opl > 5000) {
				
					$skidka_status = 1;
				}
				
				if ($summa_opl == 2000) {
					
					$skidka_status = 1;
				}
				
				$model_contracts = Contracts::find()->where(['auto_id' => $auto_id])->all();
			
				$contract_id = 0;
				
				$driver_id = 0;
				
				if (!empty($model_contracts)) {
					
					
					foreach ($model_contracts as $cntrct) {
					    
					    
					    $cntrct_date_start = $cntrct->date_pay_arenda;
					    $cntrct_date_end = $cntrct->date_end;


                        if (strtotime($date_narush) >= strtotime($cntrct_date_start) 
                            && strtotime($date_narush) <= strtotime($cntrct_date_end)) 
                        {
                            $driver_id = $cntrct->driver_id;
					        $contract_id = $cntrct->id_contract;
					        break;
                        }						    
					    
					}
					
				}
			
			
			
				$model->auto_id = $auto_id;
				$model->param_id = $id_param;
				$model->contract_id = $contract_id;
				$model->driver_id = $driver_id;
				
				
				$model->n_post = $n_post;
				$model->date_post = $date_post;
				$model->date_narush = $date_narush;
				$model->type_opl = $type_opl;
				$model->type_opl_api = $type_opl_api;
				$model->summa_opl = $summa_opl;
				$model->skidka_status = $skidka_status;
				$model->narushitel = $narushitel;
				
				$model->save(false);
				
				
				
				
			} else {
				
				
				//Пробуем по машине найти договор и водителя
				
				
				if (!empty($model_autoparams)) {
					
					
					$auto_id = $model_autoparams[0]->auto_id;
					
					
					$id_param = $model_autoparams[0]->id_param;
					//Ищем договор, чтобы определиться с водителем и вычислениями
					$model_contracts = Contracts::find()->where(['auto_id' => $auto_id])->all();
					
					
					
			
					$contract_id = 0;
					
					$driver_id = 0;
					
					if (!empty($model_contracts)) {
						
						
						foreach ($model_contracts as $cntrct) {
						    
						    
						    $cntrct_date_start = $cntrct->date_pay_arenda;
						    $cntrct_date_end = $cntrct->date_end;


                            if (strtotime($date_narush) >= strtotime($cntrct_date_start) 
                                && strtotime($date_narush) <= strtotime($cntrct_date_end)) 
                            {
                                $driver_id = $cntrct->driver_id;
						        $contract_id = $cntrct->id_contract;
						        break;
                            }						    
						    
						}
						
					}
					
					$model = new Pddpenalties();
					
					
					if ($summa_opl > 5000) {
				
						$skidka_status = 1;
					}
					
					if ($summa_opl == 2000) {
						
						$skidka_status = 1;
					}
					
					
					$model->auto_id = $auto_id;
					$model->param_id = $id_param;
					$model->contract_id = $contract_id;
					$model->driver_id = $driver_id;
					$model->n_post = $n_post;
					$model->date_post = $date_post;
					$model->date_narush = $date_narush;
					$model->type_opl = $type_opl;
					$model->type_opl_api = $type_opl_api;
					$model->summa_opl = $summa_opl;
					$model->skidka_status = $skidka_status;
					$model->narushitel = $narushitel;
					
					
					$model->save(false);
									
					
				} 
			
			}
			
				
		}
		
		echo 123;
		
		
		
    }

	 protected function findModel($id)
    {
        if (($model = Pddpenalties::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
