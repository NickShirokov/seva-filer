CREATE  VIEW `maintable` AS 



SELECT
    `cntrcts`.`id_contract` AS `id_contract`,
    `atmbls`.`id_auto` AS `id_auto`,
    `drvrs`.`id_driver` AS `id_driver`,
    `aststs`.`status_name` AS `status_name`,
    CONCAT(
        `atmbls`.`mark`,
        ' ',
        `atmbls`.`model`,
        ' ',
        `atmbls`.`gosnomer`
    ) AS `AutoMobile`,
    `cntrcts`.`date_start` AS `date_start`,
	
	`cntrcts`.`date_pay_arenda` AS `date_pay_arenda`,
	
    CONCAT(
        `drvrs`.`first_name`,
        ' ',
        `drvrs`.`middle_name`,
        ' ',
        `drvrs`.`last_name`
    ) AS `FIO`,
	
	
	
	
    `cntrcts`.`arenda_stoim` AS `arenda_stoim`,
	
    IFNULL(SUM(`pmnts`.`arenda`),
    0) AS `ArendaSumma`,
    IFNULL(SUM(`pmnts`.`summa`),
    0) AS `MainSumma`,
    ROUND(
        (
            IFNULL(SUM(`pmnts`.`arenda`),
            0) / `cntrcts`.`arenda_stoim`
        ),
        0
    ) AS `ArendaDays`,
    (
        TO_DAYS(CURDATE()) - TO_DAYS(`cntrcts`.`date_pay_arenda`)) AS `FullDays`,
		
		
		
        IFNULL(SUM(`pmnts`.`pay_dogovor`),
        0) AS `PayDogovorSumma`,
        (
        SELECT
            IFNULL(SUM(`our_penalties`.`pen_summa`),
            0)
        FROM
            `our_penalties`
        WHERE
            (
                `our_penalties`.`p_contract_id` = `cntrcts`.`id_contract` 
				AND `our_penalties`.`status_o_pen` = 1
            )
    ) AS `SummaOurPenalties`,
    (
        (
            (
            SELECT
                IFNULL(SUM(`our_penalties`.`pen_summa`),
                0)
            FROM
                `our_penalties`
            WHERE
                (
                    `our_penalties`.`p_contract_id` = `cntrcts`.`id_contract`
					AND `our_penalties`.`status_o_pen` = 1
                )
        ) + `cntrcts`.`p_vznos`
        ) + `cntrcts`.`vykup`
    ) AS `SummaOurPenaltiesPvznosVykup`,
	
	
	
    (
(
			
			`cntrcts`.`p_vznos` +
			`cntrcts`.`vykup`
			
			- 
			(
				SELECT IFNULL(SUM(`payments`. `pay_dogovor`), 0) 
				FROM `payments`
				WHERE `payments`.`contract_id` = `cntrcts`.`id_contract`
			)
			
			+ 
			
			(
				SELECT IFNULL(SUM(`our_penalties`.`pen_summa`), 0)
				FROM `our_penalties`
				WHERE `our_penalties`.`p_contract_id` = `cntrcts`.`id_contract`
			
			)
			
			- 
			
			(
				SELECT IFNULL(SUM(`payments`.`pay_ourpen`),0)
				FROM `payments`
				WHERE `payments`.`contract_id` = `cntrcts`.`id_contract`
			)
			
			
			
			
		)
    ) AS `DolgPoDogovoru`,
	
	
	
	
	
    IFNULL(SUM(`pmnts`.`prostoy_days`),
    0) AS `ProstoyDays`,
	
    IFNULL(
        SUM(
            (
                `pmnts`.`prostoy_days` * `pmnts`.`prostoy_summa`
            )
        ),
        0
    ) AS `ProstoySumma`,
	
	
	
	
	#Пробуем просуммировать простои
	(
	(
	
		SELECT IFNULL (SUM(`prostoys`.`summa_prostoy` * `prostoys`.`days_prostoy`), 0)
		FROM `prostoys`
		WHERE (`prostoys`.`contract_id` = `cntrcts`.`id_contract`)
	
	)  - (
	
		SELECT IFNULL (SUM(`payments`.`prostoy_summa`), 0)
		FROM `payments`
		WHERE (`payments`.`contract_id` = `cntrcts`.`id_contract`)
	
	
		)
	
	
	
	) AS `ProstiysDolg`,
	
	
	
	
	
	
	
	
    (
    SELECT
        IFNULL(SUM(`pdd_penalties`.`summa_opl`),
        0)
    FROM
        `pdd_penalties`
    WHERE
        (
            (
                `pdd_penalties`.`contract_id` = `cntrcts`.`id_contract`
            ) AND(`pdd_penalties`.`type_opl` = 1)
        )
) AS `SummaPddPenalties`,


(
    SELECT
        IFNULL(
            SUM(`pdd_real_penalties`.`summa_real`),
            0
        )
    FROM
        `pdd_real_penalties`
    WHERE
        (
            (
                `pdd_real_penalties`.`contract_id` = `cntrcts`.`id_contract`
            ) AND(
                `pdd_real_penalties`.`type_opl` <> 2
            )
        )
) AS `SummaPddPenaltiesReal`,
IFNULL(SUM(`pmnts`.`pay_pdd`),
0) AS `PayPddSumma`,




(
    (
		SELECT
			IFNULL(SUM(`pdd_penalties`.`summa_opl`),
			0)
		FROM
			`pdd_penalties`
		WHERE
			(
				(
					`pdd_penalties`.`contract_id` = `cntrcts`.`id_contract`
				) AND(`pdd_penalties`.`type_opl` <> 2)
			)
	) 

	
) AS `DolgPddSumma`,




(

	
	(
		SELECT
			IFNULL(
				SUM(`pdd_real_penalties`.`summa_real`),
				0
			)
		FROM
			`pdd_real_penalties`
		WHERE
			(
				(
					`pdd_real_penalties`.`contract_id` = `cntrcts`.`id_contract`
				) AND(
					`pdd_real_penalties`.`type_opl` <> 2
				)
			)
	) 
	
	
	-

	
	(
	
		SELECT 
		IFNULL(SUM(`pments`.`pay_pdd`), 0)
		FROM `payments` `pments`
		LEFT JOIN `pdd_penalties` `pddpntls`	
		ON `pddpntls`.`id_shtraf` = `pments`.`id_pdd_shtraf`
		WHERE `pments`.`contract_id` = `cntrcts`.`id_contract`
		AND `pddpntls`.`type_opl` <> 2
 	)
	
) 


AS `DolgPddSummaReal`,



(
    (
        IFNULL(SUM(`pmnts`.`summa`),
        0) -(
            ROUND(
                (
                    IFNULL(SUM(`pmnts`.`arenda`),
                    0) / `cntrcts`.`arenda_stoim`
                ),
                0
            ) * `cntrcts`.`arenda_stoim`
        )
    ) - IFNULL(SUM(`pmnts`.`pay_dogovor`),
    0)
) AS `Ostatok`,



FLOOR(
    (
        (
            (
                IFNULL(SUM(`pmnts`.`summa`),
                0) -(
                    ROUND(
                        (
                            IFNULL(SUM(`pmnts`.`arenda`),
                            0) / `cntrcts`.`arenda_stoim`
                        ),
                        0
                    ) * `cntrcts`.`arenda_stoim`
                )
            ) - IFNULL(SUM(`pmnts`.`pay_dogovor`),
            0)
        ) / `cntrcts`.`arenda_stoim`
    )
) AS `PereraschetDays`,
(
    FLOOR(
        (
            (
                (
                    IFNULL(SUM(`pmnts`.`summa`),
                    0) -(
                        ROUND(
                            (
                                IFNULL(SUM(`pmnts`.`arenda`),
                                0) / `cntrcts`.`arenda_stoim`
                            ),
                            0
                        ) * `cntrcts`.`arenda_stoim`
                    )
                ) - IFNULL(SUM(`pmnts`.`pay_dogovor`),
                0)
            ) / `cntrcts`.`arenda_stoim`
        )
    ) * `cntrcts`.`arenda_stoim`
) AS `PereraschetDaysArendSumma`,
(
    (
        (
            IFNULL(SUM(`pmnts`.`summa`),
            0) 
			
			-
		
			(
				SELECT IFNULL(SUM(payments.summa),0) 
				FROM payments
				WHERE payments.prostoy_summa > 0 AND payments.contract_id = cntrcts.id_contract
			)
			
			
			- (
                ROUND(
                    (
                        IFNULL(SUM(`pmnts`.`arenda`),
                        0) / `cntrcts`.`arenda_stoim`
                    ),
                    0
                ) * `cntrcts`.`arenda_stoim`
            )
        ) - IFNULL(SUM(`pmnts`.`pay_dogovor`) 
		
		
		
		,
        0)
    ) -(
        FLOOR(
            (
                (
                    (
                        IFNULL(SUM(`pmnts`.`summa`),
                        0) -(
                            ROUND(
                                (
                                    IFNULL(SUM(`pmnts`.`arenda`),
                                    0) / `cntrcts`.`arenda_stoim`
                                ),
                                0
                            ) * `cntrcts`.`arenda_stoim`
                        )
                    ) - IFNULL(SUM(`pmnts`.`pay_dogovor`),
                    0)
                ) / `cntrcts`.`arenda_stoim`
            )
        ) * `cntrcts`.`arenda_stoim`
    ) 
) AS `NewOstatok`,





	# Долг по аренде
	(
		`cntrcts`.`arenda_stoim` * (
			
			
			
			(
			TO_DAYS(CURDATE()) - TO_DAYS(`cntrcts`.`date_pay_arenda` - 1))
			
		
			
		) 
		- 	
		(
			IFNULL(SUM(`pmnts`.`arenda`),
							0)
		)

	
	)
	
	
	AS `DolgPoArende`,
	
	
       (
        `cntrcts`.`date_pay_arenda` - INTERVAL(
            -(
                ROUND(
                    (
                        IFNULL(SUM(`pmnts`.`arenda`),
                        0) / `cntrcts`.`arenda_stoim`
                    ),
                    0
                )
            ) + 2
        ) DAY
    ) AS `OplachenDo`,
   

   (

   
		# Долг по аренде
		IFNULL((
		`cntrcts`.`arenda_stoim` * (
			
			
			
			(
			TO_DAYS(CURDATE()) - TO_DAYS(`cntrcts`.`date_pay_arenda` - 1))
			
		
			
		) 
		- 	
		(
			IFNULL(SUM(`pmnts`.`arenda`),
							0)
		)

	
		), 0) + 
		
		
		# Долг по простою
		
			(
			IFNULL((
			
				SELECT IFNULL (SUM(`prostoys`.`summa_prostoy` * `prostoys`.`days_prostoy`), 0)
				FROM `prostoys`
				WHERE (`prostoys`.`contract_id` = `cntrcts`.`id_contract`)
				
			
			),0)  - IFNULL((
			
				SELECT IFNULL (SUM(`payments`.`prostoy_summa`), 0)
				FROM `payments`
				WHERE (`payments`.`contract_id` = `cntrcts`.`id_contract`)
			
			
				),0)
			
			
			
			)
				
		+
		
		# Долг по нашим штрафам и договору
   
		(
			
			IFNULL(`cntrcts`.`p_vznos`,0) +
			IFNULL(`cntrcts`.`vykup`,0)
			
			- 
			IFNULL((
				SELECT IFNULL(SUM(`payments`. `pay_dogovor`), 0) 
				FROM `payments`
				WHERE `payments`.`contract_id` = `cntrcts`.`id_contract`
			),0)
			
			+ 
			
			IFNULL((
				SELECT IFNULL(SUM(`our_penalties`.`pen_summa`),0)
				FROM `our_penalties`
				WHERE `our_penalties`.`p_contract_id` = `cntrcts`.`id_contract`
				
			),0)
			
			- 
			
			IFNULL((
				SELECT IFNULL(SUM(`payments`.`pay_ourpen`),0)
				FROM `payments`
				WHERE `payments`.`contract_id` = `cntrcts`.`id_contract`
			),0)
			
			
			
			
		) 
		
		+ 
		
		(

	
			IFNULL((
				SELECT
					IFNULL(
						SUM(`pdd_real_penalties`.`summa_real`),
						0
					)
				FROM
					`pdd_real_penalties`
				WHERE
					(
						(
							`pdd_real_penalties`.`contract_id` = `cntrcts`.`id_contract`
						) AND(
							`pdd_real_penalties`.`type_opl` <> 2
						)
					)
			) ,0)
			
			
			-

			
			IFNULL((
			
				SELECT 
				IFNULL(SUM(`pments`.`pay_pdd`), 0)
				FROM `payments` `pments`
				LEFT JOIN `pdd_penalties` `pddpntls`	
				ON `pddpntls`.`id_shtraf` = `pments`.`id_pdd_shtraf`
				WHERE `pments`.`contract_id` = `cntrcts`.`id_contract`
				AND `pddpntls`.`type_opl` <> 2
			),0)
			
		)
	   
   
   
   
			
			
	) AS `MainDolg`,
		
		
		
		
		
        IFNULL(
            (
            SELECT
                SUM(`payments`.`arenda`)
            FROM
                `payments`
            WHERE
                (
                    (
                        `payments`.`contract_id` = `cntrcts`.`id_contract`
                    ) AND(
                        `payments`.`date_payment` >(CURDATE() - INTERVAL 7 DAY))
                    )
                ),
                0
        ) AS `WeekPayArenda`,
        IFNULL(
            (
            SELECT
                SUM(`payments`.`arenda`)
            FROM
                `payments`
            WHERE
                (
                    (
                        `payments`.`contract_id` = `cntrcts`.`id_contract`
                    ) AND(
                        `payments`.`date_payment` >(CURDATE() - INTERVAL 30 DAY))
                    )
                ),
                0
        ) AS `MonthPayArenda`
    FROM
        (
            (
                (
                    (
                        `contracts` `cntrcts`
                    LEFT JOIN `payments` `pmnts` ON
                        (
                            (
                                `pmnts`.`contract_id` = `cntrcts`.`id_contract`
                            )
                        )
                    )
					
					
					
					
					
					
                LEFT JOIN `automobiles` `atmbls` ON
                    (
                        (
                            `cntrcts`.`auto_id` = `atmbls`.`id_auto`
                        )
                    )
                )
            LEFT JOIN `auto_status` `aststs` ON
                (
                    (
                        `atmbls`.`status` = `aststs`.`id_status`
                    )
                )
            )
        LEFT JOIN `drivers` `drvrs` ON
            (
                (
                    `cntrcts`.`driver_id` = `drvrs`.`id_driver`
                )
            )
        )
		
    GROUP BY
        `cntrcts`.`id_contract`